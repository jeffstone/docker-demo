docker build -t mysql/server:5.7.23 .

docker run -d -p 3306:3306 --name mysql-server -v /var/lib/docker/mysql/server/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql/server:5.7.23

docker exec -it mysql-server mysql -uroot -p

use mysql;

GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
flush privileges;
