docker build -t centos/dubbo-admin:2.6.0 .

docker run -d \
-p 8080:8080 \
-e dubbo.registry.address=zookeeper://192.168.30.137:2181 \
-e dubbo.admin.root.password=root \
-e dubbo.admin.guest.password=guest \
centos/dubbo-admin:2.6.0

ip:8080/dubbo-admin-2.6.0/
